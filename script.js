const form = document.getElementById('form')
const input = document.getElementById('input')
const todolist = document.getElementById('todolist')

const todoStorage = JSON.parse(localStorage.getItem('todos'))

if (todoStorage) {
    todoStorage.forEach(todo => {
        addTodo(todo)
    })
}

form.addEventListener('submit', (e) => {
    e.preventDefault()

    addTodo()
})

function addTodo(todo) {
    let todo_text = input.value;

    if (todo) {
        todo_text = todo.text
    }

    if (todo_text) {
        const todo_element = document.createElement('li')
        todo_element.innerText = todo_text
        todolist.appendChild(todo_element)
        input.value = ''
        if (todo && todo.completed) {
            todo_element.classList.add('completed')
        }


        todo_element.addEventListener('click', () => {
            todo_element.classList.toggle('completed')
            updateStorage()
        })

        todo_element.addEventListener('contextmenu', (e) => {
            e.preventDefault()

            todo_element.remove()
            updateStorage()
        })

        updateStorage()
    }
};

function updateStorage() {
    const todos_el = document.querySelectorAll('li');
    const todos = []

    todos_el.forEach(todo => {
        todos.push({
            text: todo.innerText,
            completed: todo.classList.contains('completed')
        })
    })

    localStorage.setItem('todos', JSON.stringify(todos))
}